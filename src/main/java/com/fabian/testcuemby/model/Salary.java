package com.fabian.testcuemby.model;

public class Salary extends Model {

	protected String name;
	protected float value;
	
	public Salary() {
		super();
	}
	
	public Salary(int id, String name, float value) {
		super();
		this.id = id;
		this.name = name;
		this.value = value;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the value
	 */
	public float getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(float value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Salary [id=" + id + ", name=" + name + ", value=" + value + "]";
	}
	
}
