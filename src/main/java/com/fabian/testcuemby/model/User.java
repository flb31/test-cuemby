package com.fabian.testcuemby.model;

import java.text.ParseException;
import java.util.Date;

import com.fabian.testcuemby.util.FormatData;

public class User extends Model {

	protected String identification;
	protected String name;
	protected String lastName;
	protected int age;
	protected Position position;
	protected int positionID;
	protected Date birthday;
	protected Date dateEntryCompany;
	protected String status;
	
	// Status user
	public static final String ACTIVE = "active";
	public static final String INACTIVE = "inactive";
	
	public User() {
		super();
	}
	
	public User(int id, String identification, String name, String lastName, Position position, String birthday, String dateEntryCompany, String status, int positionID) throws ParseException {
		super();
		this.id = id;
		this.identification = identification;
		this.name = name;
		this.lastName = lastName;
		this.position = position;
		this.setBirthday(birthday);
		this.setDateEntryCompany(dateEntryCompany);
		this.status = status;
		this.positionID = positionID; 
	}
	
	/**
	 * @param identification the identification to set
	 */
	public String getIdentification() {
		return identification;
	}
	
	/**
	 * @return the name
	 */
	public void setIdentification(String identification) {
		this.identification = identification;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}


	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	/**
	 * @return the age
	 */
	public int getAge() {
		this.age = FormatData.calculateYears(this.birthday);
		return age;
	}


	/**
	 * @return the position
	 */
	public Position getPosition() {
		return position;
	}


	/**
	 * @param position the position to set
	 */
	public void setPosition(Position position) {
		this.position = position;
	} 


	/**
	 * @return the birthday
	 */
	public String getBirthday() {
		if(FormatData.isValidDate(birthday)) {
			return FormatData.dateToString(birthday);			
		}
		return "";
	}


	/**
	 * @param birthday the birthday to set
	 * @throws ParseException 
	 */
	public void setBirthday(String birthdayStr) throws ParseException {
		if(FormatData.isValidDate(birthdayStr)) {
			Date birthday = FormatData.stringToDate(birthdayStr);
			this.birthday = birthday;
		}
	}


	/**
	 * @return the dateEntryCompany
	 */
	public String getDateEntryCompany() {
		if(FormatData.isValidDate(dateEntryCompany)) {
			return FormatData.dateToString(dateEntryCompany);
		}
		return "";
	}


	/**
	 * @param dateEntryCompany the dateEntryCompany to set
	 * @throws ParseException 
	 */
	public void setDateEntryCompany(String dateEntryCompanyStr) throws ParseException {
		if(FormatData.isValidDate(dateEntryCompanyStr)) {
			Date dateEntryCompany = FormatData.stringToDate(dateEntryCompanyStr);
			this.dateEntryCompany = dateEntryCompany;
		}
	}


	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}


	/**
	 * @param position the position to set
	 */
	public void setPositionID(int positionID) {
		this.positionID = positionID;
	}
	
	
	/**
	 * @return the position
	 */
	public int getPositionID() {
		return positionID;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getFullName() {
		return this.name + " " + this.lastName;
	}


	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", lastName=" + lastName + ", age=" + age + ", position="
				+ position + ", birthday=" + birthday + ", dateEntryCompany=" + dateEntryCompany + ", status=" + status
				+ "]";
	}
}
