package com.fabian.testcuemby.model;

import java.text.ParseException;
import java.util.Date;

import com.fabian.testcuemby.util.FormatData;

public class Assistance extends Model {

	protected int userID;
	protected User user;
	protected Date registerDate;
	
	public Assistance() {
		
	}
	
	public Assistance(int id, User user, String registerDate, int userID) throws ParseException {
		super();
		this.id = id;
		this.user = user;
		this.userID = userID;
		this.setRegisterDate(registerDate);
	}
	/**
	 * @return the userID
	 */
	public User getUser() {
		return user;
	}
	/**
	 * @param userID the userID to set
	 */
	public void setUserID(User user) {
		this.user = user;
	}
	/**
	 * @return the registerDate
	 */
	public String getRegisterDate() {
		if(FormatData.isValidDate(registerDate)) {
			return FormatData.dateToString(registerDate);
		}
		return "";
	}

	/**
	 * @param registerDate the registerDate to set
	 * @throws ParseException 
	 */
	public void setRegisterDate(String registerDate) throws ParseException {
		if(FormatData.isValidDate(registerDate)) {
			Date regirterDate = FormatData.stringToDate(registerDate);
			this.registerDate = regirterDate;
		}
	}

	public int getUserID() {
		return userID;
	}
	public void setUserID(int userID) {
		this.userID = userID;
	}
	
	public void setUser(User user) {
		this.user = user;
	}
	@Override
	public String toString() {
		return "Assistance [id=" + id + ", user=" + user.getFullName()+ ", registerDate=" + registerDate + "]";
	}
	
}
