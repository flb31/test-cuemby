package com.fabian.testcuemby.model;

public class Position extends Model {
	
	protected String name;
	protected Salary salary;
	
	public Position(int id, String name, Salary salary) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the salary
	 */
	public Salary getSalary() {
		return salary;
	}
	/**
	 * @param salary the salary to set
	 */
	public void setSalary(Salary salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Position [id=" + id + ", name=" + name + ", salary=" + salary + "]";
	}
	
}
