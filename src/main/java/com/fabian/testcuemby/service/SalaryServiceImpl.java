package com.fabian.testcuemby.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fabian.testcuemby.dao.SalaryDao;
import com.fabian.testcuemby.model.Salary;

@Service("salaryService")
public class SalaryServiceImpl implements SalaryService {
	
	@Autowired
	private SalaryDao _salaryDao;

	@Override
	public void saveSalary(Salary salary) {
		// TODO Auto-generated method stub
		_salaryDao.saveSalary(salary);
	}

	@Override
	public void deleteSalaryById(int salaryID) {
		// TODO Auto-generated method stub
		_salaryDao.deleteSalaryById(salaryID);
	}

	@Override
	public void updateSalary(Salary salary) {
		// TODO Auto-generated method stub
		_salaryDao.updateSalary(salary);
	}

	@Override
	public List<Salary> findAllSalarys() {
		// TODO Auto-generated method stub
		return _salaryDao.findAllSalarys();
	}

	@Override
	public Salary findById(int salaryID) {
		// TODO Auto-generated method stub
		return _salaryDao.findById(salaryID);
	}

}
