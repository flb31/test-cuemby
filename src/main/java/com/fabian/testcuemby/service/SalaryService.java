package com.fabian.testcuemby.service;

import java.util.List;

import com.fabian.testcuemby.model.Salary;

public interface SalaryService {
	void saveSalary(Salary salary);
	
	void deleteSalaryById(int salaryID);
	
	void updateSalary(Salary salary);
	
	List<Salary> findAllSalarys();
	
	Salary findById(int salaryID);
}
