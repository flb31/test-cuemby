package com.fabian.testcuemby.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fabian.testcuemby.dao.UserDao;
import com.fabian.testcuemby.model.Assistance;
import com.fabian.testcuemby.model.User;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao _userDao;
	
	@Override
	public void saveUser(User user) {
		// TODO Auto-generated method stub
		_userDao.saveUser(user);
	}

	@Override
	public void deleteUserById(int userID) {
		// TODO Auto-generated method stub
		_userDao.deleteUserById(userID);
	}

	@Override
	public boolean updateUser(User newUser, List<Assistance> assistance) {
		// TODO Auto-generated method stub
		return _userDao.updateUser(newUser, assistance);
	}

	@Override
	public List<User> findAllUsers() {
		// TODO Auto-generated method stub
		return _userDao.findAllUsers();
	}

	@Override
	public User findById(int userID) {
		// TODO Auto-generated method stub
		return _userDao.findById(userID);
	}

	@Override
	public User findByIdentification(String identification) {
		// TODO Auto-generated method stub
		return _userDao.findByIdentification(identification);
	}

}
