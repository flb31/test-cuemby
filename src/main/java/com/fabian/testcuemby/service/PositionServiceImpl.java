package com.fabian.testcuemby.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fabian.testcuemby.dao.PositionDao;
import com.fabian.testcuemby.model.Position;

@Service("positionService")
public class PositionServiceImpl implements PositionService {

	@Autowired
	private PositionDao _positionDao;
	
	@Override
	public void savePosition(Position position) {
		// TODO Auto-generated method stub
		_positionDao.savePosition(position);
	}

	@Override
	public boolean deletePositionById(int positionID) {
		// TODO Auto-generated method stub
		return _positionDao.deletePositionById(positionID);
	}

	@Override
	public boolean updatePosition(Position position) {
		// TODO Auto-generated method stub
		return _positionDao.updatePosition(position);
	}

	@Override
	public List<Position> findAllPositions() {
		// TODO Auto-generated method stub
		return _positionDao.findAllPositions();
	}

	@Override
	public Position findById(int positionID) {
		// TODO Auto-generated method stub
		return _positionDao.findById(positionID);
	}

}
