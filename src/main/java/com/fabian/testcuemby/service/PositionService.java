package com.fabian.testcuemby.service;

import java.util.List;

import com.fabian.testcuemby.model.Position;

public interface PositionService {
	void savePosition(Position position);
	
	boolean deletePositionById(int positionID);
	
	boolean updatePosition(Position position);
	
	List<Position> findAllPositions();
	
	Position findById(int positionID);
}
