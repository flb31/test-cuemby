package com.fabian.testcuemby.service;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fabian.testcuemby.dao.AssistanceDao;
import com.fabian.testcuemby.model.Assistance;
import com.fabian.testcuemby.model.User;

@Service("assistanceService")
public class AssistanceServiceImpl implements AssistanceService {

	@Autowired
	private AssistanceDao _asDao;
	
	@Override
	public void saveAssistance(Assistance assistance) {
		// TODO Auto-generated method stub
		_asDao.saveAssistance(assistance);
	}

	@Override
	public List<User> findAllAssistancesSince(String since, String until) throws ParseException {
		// TODO Auto-generated method stub
		return _asDao.findAllAssistancesSince(since, until);
	}

	@Override
	public List<Assistance> findAllAssistances() {
		// TODO Auto-generated method stub
		return _asDao.findAllAssistances();
	}

}
