package com.fabian.testcuemby.controller;

import java.util.List;
import java.text.ParseException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import com.fabian.testcuemby.configuration.Configuration;
import com.fabian.testcuemby.model.Assistance;
import com.fabian.testcuemby.model.User;
import com.fabian.testcuemby.service.AssistanceService;
import com.fabian.testcuemby.service.UserService;
import com.fabian.testcuemby.util.CustomMessageType;
import com.fabian.testcuemby.util.FormatData;
import com.fabian.testcuemby.util.Validator;

@Controller
@RequestMapping("/v1")
public class AssistanceController extends Validator {
	
	private final String URI_VERSION = "/v1";
	private final String URI = "/assistance";
	
	@Autowired
	AssistanceService _aService;
	
	@Autowired
	UserService _usService;
	
	
	// Post
	@CrossOrigin(origins = Configuration.ORIGIN)
	@RequestMapping(value=URI, method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> create(@RequestBody Assistance assistance, UriComponentsBuilder uriComponentsBuilder) {
		
		String res = validations(assistance);
		if(res != null) {
			return new ResponseEntity<>(new CustomMessageType(res), HttpStatus.CONFLICT);
		}

		// Save
		int assistanceID = FormatData.randomID();
		assistance.setId(assistanceID);
		assistance.setUser(_usService.findById(assistance.getUserID()));
		_aService.saveAssistance(assistance);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path(String.format("%s%s/{id}", URI_VERSION, URI))
				.buildAndExpand(assistanceID)
				.toUri()
				);
		
		return new ResponseEntity<>(new CustomMessageType(message(MESSAGE_SUCCESS, "Assistance")), headers, HttpStatus.CREATED);
	}
	
	// Get Assistance
	@CrossOrigin(origins = Configuration.ORIGIN)
	@RequestMapping(value=URI, method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<?> filter(@RequestParam(value="since", required=true) String since, @RequestParam(value="until", required=true) String until) {
		List<User> users = new ArrayList<>();
		
		
		//Validation
		if(!dateddMMYYYY(since, true)) {
			return new ResponseEntity<>(new CustomMessageType(message(MESSAGE_NOT_VALID, "Date Since", "")), HttpStatus.CONFLICT);
		}
		
		if(!dateddMMYYYY(until, true)) {
			return new ResponseEntity<>(new CustomMessageType(message(MESSAGE_NOT_VALID, "Date Until", "")), HttpStatus.CONFLICT);
		}
		
		// Different dates
		if(since.equals(until)) {
			return new ResponseEntity<>(new CustomMessageType("Since date should be different than until date"), HttpStatus.CONFLICT);
		}
		
		// Since minor than until
		try {
			if(!FormatData.stringToDate(since).before( FormatData.stringToDate(until) )) {
				return new ResponseEntity<>(new CustomMessageType("Since date should be minor than Until date."), HttpStatus.CONFLICT);
			}
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		try {
			users = _aService.findAllAssistancesSince(since, until);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.OK);
		}
		
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
	
	protected String validations(Assistance assistance) {
		if( !dateddMMYYYY(assistance.getRegisterDate(), true) ) {
			return message(MESSAGE_NOT_VALID, "Date Register", "");
		}
		
		// User exist
		if(_usService.findById(assistance.getUserID()) == null) {
			return message(MESSAGE_NOT_FOUND, "User", "");
		}
		
		return null;
	}
}
