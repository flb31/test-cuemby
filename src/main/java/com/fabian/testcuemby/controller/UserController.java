package com.fabian.testcuemby.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.UriComponentsBuilder;

import com.fabian.testcuemby.service.AssistanceService;
import com.fabian.testcuemby.service.PositionService;
import com.fabian.testcuemby.service.UserService;
import com.fabian.testcuemby.util.CustomMessageType;
import com.fabian.testcuemby.util.FormatData;
import com.fabian.testcuemby.util.Validator;
import com.fabian.testcuemby.configuration.Configuration;
import com.fabian.testcuemby.model.Position;
import com.fabian.testcuemby.model.User;

@Controller
@RequestMapping("/v1")
public class UserController extends Validator {
	
	private final String URI_VERSION = "/v1";
	private final String URI = "/user";
	
	@Autowired
	UserService _uService;
	
	@Autowired
	PositionService _pService;
	
	@Autowired
	AssistanceService _asService;
	
	
	// Get all
	@CrossOrigin(origins = Configuration.ORIGIN)
	@RequestMapping(value=URI, method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<User>> show() {
		List<User> users = new ArrayList<>();
		
		users = _uService.findAllUsers(); 
		if(users.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<User>>(users, HttpStatus.OK);
	}
	
	// Post
	@CrossOrigin(origins = Configuration.ORIGIN)
	@RequestMapping(value=URI, method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> create(@RequestBody User user, UriComponentsBuilder uriComponentsBuilder) {
		
		if(_uService.findByIdentification(user.getIdentification()) != null) {
			return new ResponseEntity<>(new CustomMessageType(message(MESSAGE_EXIST, "User", "")), HttpStatus.CONFLICT);
		}
		
		String res = validations(user);
		if(res != null) {
			return new ResponseEntity<>(new CustomMessageType(res), HttpStatus.CONFLICT);
		}

		// Save
		Position position = _pService.findById(user.getPositionID());
		int userID = FormatData.randomID();
		user.setId(userID);
		user.setPosition(position);
		_uService.saveUser(user);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path(String.format("%s%s/{id}", URI_VERSION, URI))
				.buildAndExpand(userID)
				.toUri()
				);
		
		return new ResponseEntity<>(new CustomMessageType(message(MESSAGE_SUCCESS, "User")), headers, HttpStatus.CREATED);
	}
	
	// PATCH
	@CrossOrigin(origins = Configuration.ORIGIN)
	@RequestMapping(value=URI+"/{id}", method = RequestMethod.PATCH, headers = "Accept=application/json")
	public ResponseEntity<?> update(@PathVariable("id") int userID, @RequestBody User updatedUser) {
		
		User currentUser = _uService.findById(userID);
		if(currentUser == null) {
			return new ResponseEntity<>(new CustomMessageType(message(MESSAGE_NOT_FOUND, "User")), HttpStatus.CREATED);
		}
		
		User userById = _uService.findByIdentification(updatedUser.getIdentification());
		if( userById != null && userById.getId() != updatedUser.getId() ) {
			return new ResponseEntity<>(new CustomMessageType(message(MESSAGE_EXIST, "Identification", "in other User. can't update.")), HttpStatus.CONFLICT);
		}
		
		//Verify ids
		if(updatedUser.getId() != currentUser.getId()) {
			return new ResponseEntity<>(new CustomMessageType(message(MESSAGE_IDS, "User")), HttpStatus.CREATED);
		}
		
		String res = validations(updatedUser);
		if(res != null) {
			return new ResponseEntity<>(new CustomMessageType(res), HttpStatus.CONFLICT);
		}
		
		// Update
		Position position = _pService.findById(updatedUser.getPositionID());
		updatedUser.setPosition(position);
		boolean updated = _uService.updateUser(updatedUser, _asService.findAllAssistances());
		final String updatedStr = (updated) ? MESSAGE_UPDATE_SUCCESS : MESSAGE_NOT_UPDATED;
		
		return new ResponseEntity<>(new CustomMessageType(message(updatedStr, "User")), HttpStatus.OK);
	}
	
	
	// Validations fields
	protected String validations(User user) {
		if( !minCharacters(user.getIdentification(), true, MIN_CHAR_ID)  ) {
			return message(MESSAGE_MIN_REQUIRED, "Identification", MIN_CHAR_ID);
		}
		
		if( !text(user.getName(), true) ) {
			return message(MESSAGE_NOT_VALID, "Name", "Not use special characters.");
		}
		
		if( !empty(user.getName(), true) ) {
			return message(MESSAGE_MIN_REQUIRED, "Name", 3);
		}
		
		if( !text(user.getLastName(), true) ) {
			return message(MESSAGE_NOT_VALID, "Last Name", "Not use special characters.");
		}
		
		if( !empty(user.getLastName(), true) ) {
			return message(MESSAGE_MIN_REQUIRED, "Last Name", 3);
		}
		
		if( _pService.findById(user.getPositionID()) == null ) {
			return message(MESSAGE_NOT_VALID, "Position ID", "");
		}
		
		if( !dateddMMYYYY(user.getBirthday(), true) ) {
			return message(MESSAGE_NOT_VALID, "Birthday", "");
		}
		
		if( !dateddMMYYYY(user.getDateEntryCompany(), true) ) {
			return message(MESSAGE_NOT_VALID, "Date Entry Company", "");
		}
		
		if(! (User.ACTIVE.equals( user.getStatus() ) || User.INACTIVE.equals( user.getStatus() ) )) {
			return message(MESSAGE_NOT_VALID, "Status", String.format(" should be %s or %s", User.ACTIVE, User.INACTIVE ));
		}
		
		return null;
	}
}
