package com.fabian.testcuemby.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.util.UriComponentsBuilder;

import com.fabian.testcuemby.model.Salary;
import com.fabian.testcuemby.service.SalaryService;
import com.fabian.testcuemby.util.FormatData;

@Controller
@RequestMapping("/v1")
public class SalaryController {
	
	@Autowired
	SalaryService _salaryService;
	
	@RequestMapping(value="/salary", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Salary>> getSalaries() {
		List<Salary> salaries = new ArrayList<>();
		
		salaries = _salaryService.findAllSalarys(); 
		if(salaries.isEmpty()) {
			return new ResponseEntity(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Salary>>(salaries, HttpStatus.OK);
	}
	
	@RequestMapping(value="/salary", method = RequestMethod.POST, headers = "Accept=application/json" )
	public ResponseEntity<?> createSocialMedia(@RequestBody Salary salary, 
			UriComponentsBuilder uriComponentsBuilder) {
		if(salary.getName().equals("") || salary.getName().isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		
		if(_salaryService.findById(salary.getId()) != null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		
		salary.setId(FormatData.randomID());
		_salaryService.saveSalary(salary);
		
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/v1/salary/{id}")
				.buildAndExpand(10)
				.toUri()
				);
		
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}
}
