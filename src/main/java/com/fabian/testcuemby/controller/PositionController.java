package com.fabian.testcuemby.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fabian.testcuemby.configuration.Configuration;
import com.fabian.testcuemby.model.Position;
import com.fabian.testcuemby.service.PositionService;

@Controller
@RequestMapping("/v1")
public class PositionController {
	private final String URI_VERSION = "/v1";
	private final String URI = "/position";
	
	@Autowired
	PositionService _posService;
	
	// Get all
	@CrossOrigin(origins = Configuration.ORIGIN)
	@RequestMapping(value=URI, method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Position>> show() {
		List<Position> positions = new ArrayList<>();
		
		positions = _posService.findAllPositions(); 
		if(positions.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		return new ResponseEntity<List<Position>>(positions, HttpStatus.OK);
	}
}
