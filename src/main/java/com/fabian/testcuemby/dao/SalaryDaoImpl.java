package com.fabian.testcuemby.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.fabian.testcuemby.model.Salary;
import com.fabian.testcuemby.persistence.SalaryPersistence;

@Repository
public class SalaryDaoImpl extends AbstractModel implements SalaryDao {

	private List<Salary> salaries;
	
	public SalaryDaoImpl() {
		super();
		SalaryPersistence sp = new SalaryPersistence(); 
		salaries = sp.getSalaries();
	}
	
	@Override
	public void saveSalary(Salary salary) {
		// TODO Auto-generated method stub
		salaries.add(salary);
	}

	@Override
	public void deleteSalaryById(int salaryID) {
		// TODO Auto-generated method stub
		
		int index = getIndex(salaries, salaryID); 
		salaries.remove(index);
	}

	@Override
	public void updateSalary(Salary newSalary) {
		// TODO Auto-generated method stub
		int index = getIndex(salaries, newSalary.getId()); 
		salaries.set(index, newSalary);
	}

	@Override
	public List<Salary> findAllSalarys() {
		// TODO Auto-generated method stub
		return salaries;
	}

	@Override
	public Salary findById(int salaryID) {
		// TODO Auto-generated method stub
		return (Salary)getModel(salaries, salaryID);
	}

}
