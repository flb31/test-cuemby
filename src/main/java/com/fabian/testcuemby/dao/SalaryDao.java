package com.fabian.testcuemby.dao;

import java.util.List;

import com.fabian.testcuemby.model.Salary;

public interface SalaryDao {
	void saveSalary(Salary salary);
	
	void deleteSalaryById(int salaryID);
	
	void updateSalary(Salary salary);
	
	List<Salary> findAllSalarys();
	
	Salary findById(int salaryID);
}
