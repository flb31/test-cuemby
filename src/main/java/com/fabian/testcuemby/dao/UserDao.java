package com.fabian.testcuemby.dao;

import java.util.Date;
import java.util.List;

import com.fabian.testcuemby.model.Assistance;
import com.fabian.testcuemby.model.User;

public interface UserDao {
	void saveUser(User user);
	
	void deleteUserById(int userID);
	
	boolean updateUser(User newUser, List<Assistance> assistance);
	
	List<User> findAllUsers();
	
	User findById(int userID);
	
	User findByIdentification(String identification);

}
