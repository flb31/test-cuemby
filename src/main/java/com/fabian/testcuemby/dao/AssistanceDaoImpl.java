package com.fabian.testcuemby.dao;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.fabian.testcuemby.model.Assistance;
import com.fabian.testcuemby.model.User;
import com.fabian.testcuemby.persistence.AssistancePersistence;
import com.fabian.testcuemby.util.FormatData;

@Repository
public class AssistanceDaoImpl implements AssistanceDao {

	private List<Assistance> assistances;
	
	public AssistanceDaoImpl() {
		super();
		AssistancePersistence ap = new AssistancePersistence();
		assistances = ap.getAssistances();
	}
	
	@Override
	public void saveAssistance(Assistance assistance) {
		// TODO Auto-generated method stub
		assistances.add(assistance);
	}
	
	

	@Override
	public List<User> findAllAssistancesSince(String since, String until) throws ParseException {
		// TODO Auto-generated method stub
		List<User> listFiltered = new ArrayList<>();
		Iterator<?> i = assistances.iterator();
		while (i.hasNext()) {
			Assistance assistance = (Assistance) i.next();
			
			//Parse dates
			Date sinceDate = FormatData.stringToDate(since);
			Date untilDate = FormatData.stringToDate(until);
			
			
			// Compare date for detect between since and until and user is active
			if ( validateAssistance(sinceDate, untilDate, assistance) && !hasUser(listFiltered, assistance.getUser()) ) {
				listFiltered.add(assistance.getUser());
			}
		}
		
		return listFiltered;
	}
	
	protected boolean hasUser(List<User> list, User user) {
		Iterator<?> i = list.iterator();
		while (i.hasNext()) {
			User currentUser = (User) i.next();
			if(currentUser.getId() == user.getId()) {
				return true;
			}
		}
		return false;
	}
	
	protected boolean validateAssistance(Date sinceDate, Date untilDate, Assistance assistance) throws ParseException {
		Date assReg = FormatData.stringToDate(assistance.getRegisterDate());
		return assReg.after(sinceDate) && assReg.before(untilDate) && User.ACTIVE.equals(assistance.getUser().getStatus());
	}

	@Override
	public List<Assistance> findAllAssistances() {
		// TODO Auto-generated method stub
		return assistances;
	}

}
