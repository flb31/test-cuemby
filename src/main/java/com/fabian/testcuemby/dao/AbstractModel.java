package com.fabian.testcuemby.dao;

import java.util.Iterator;
import java.util.List;

import com.fabian.testcuemby.model.Model;

public abstract class AbstractModel {
	
	protected boolean isThisModel(Model model, int modelID) {
		return model.getId() == modelID;
	}
	
	protected Model getModel(List<?> models, int modelID) {
		Iterator<?> i = models.iterator();
		while (i.hasNext()) {
			Model model = (Model) i.next();
			if(isThisModel(model, modelID)) {
				return model; 
			}
		}
		return null;
	}
	
	protected int getIndex(List<?> models, int modelID) {
		Iterator<?> i = models.iterator();
		int index = 0;
		while (i.hasNext()) {
			Model model = (Model) i.next();
			if(isThisModel(model, modelID)) {
				return index; 
			}
			index++;
		}
		return -1;
	}
}
