package com.fabian.testcuemby.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.fabian.testcuemby.model.Position;
import com.fabian.testcuemby.persistence.PositionPersistence;

@Repository
public class PositionDaoImpl extends AbstractModel implements PositionDao {

	private List<Position> positions;
	
	public PositionDaoImpl() {
		PositionPersistence pp = new PositionPersistence();
		positions = pp.getPositions();
	}
	
	@Override
	public void savePosition(Position position) {
		// TODO Auto-generated method stub
		positions.add(position);
	}

	@Override
	public boolean deletePositionById(int positionID) {
		// TODO Auto-generated method stub
		int index = getIndex(positions, positionID);
		if(index >= 0) {
			positions.remove(index);
			return true;
		}
		return false;
	}

	@Override
	public boolean updatePosition(Position newPosition) {
		// TODO Auto-generated method stub
		int index = getIndex(positions, newPosition.getId());
		if(index >= 0) {
			positions.set(index, newPosition);
			return true;
		}
		return false;
	}

	@Override
	public List<Position> findAllPositions() {
		// TODO Auto-generated method stub
		return positions;
	}

	@Override
	public Position findById(int positionID) {
		// TODO Auto-generated method stub
		return (Position)getModel(positions, positionID);
	}

}
