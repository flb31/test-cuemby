package com.fabian.testcuemby.dao;

import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.fabian.testcuemby.model.Assistance;
import com.fabian.testcuemby.model.User;
import com.fabian.testcuemby.persistence.UserPersistence;

@Repository
public class UserDaoImpl extends AbstractModel implements UserDao {

	private List<User> users;
	
	public UserDaoImpl() {
		super();
		UserPersistence up = new UserPersistence();
		users = up.getUser(); 
	}
	
	@Override
	public void saveUser(User user) {
		// TODO Auto-generated method stub
		users.add(user);
	}

	@Override
	public void deleteUserById(int userID) {
		// TODO Auto-generated method stub
		int index = getIndex(users, userID);
		users.remove(index);
	}

	@Override
	public List<User> findAllUsers() {
		// TODO Auto-generated method stub
		return users;
	}

	@Override
	public User findById(int userID) {
		// TODO Auto-generated method stub
		return (User)getModel(users, userID);
	}

	@Override
	public boolean updateUser(User newUser, List<Assistance> assistance) {
		// TODO Auto-generated method stub
		int index = getIndex(users, newUser.getId());
		if(index >= 0) {
			users.set(index, newUser);
			// synchronize data
			synchronize(assistance, newUser);
			return true;
		}
		
		return false;
	}

	@Override
	public User findByIdentification(String identification) {
		// // TODO Auto-generated method stub
		Iterator<?> i = users.iterator();
		while (i.hasNext()) {
			User user = (User) i.next();
			if(user.getIdentification().equals(identification)) {
				return user; 
			}
		}
		return null;
	}
	

	// Just by persistence
	protected void synchronize(List<Assistance> list, User updatedUser) {
		Iterator<?> i = list.iterator();
		while (i.hasNext()) {
			Assistance assistance = (Assistance) i.next();
			// Validate
			if(assistance.getUserID() == updatedUser.getId()) {
				assistance.setUser(updatedUser);
			}
		}
	}
	

}
