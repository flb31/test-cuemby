package com.fabian.testcuemby.dao;

import java.text.ParseException;
import java.util.List;

import com.fabian.testcuemby.model.Assistance;
import com.fabian.testcuemby.model.User;

public interface AssistanceDao {
	void saveAssistance(Assistance assistance);
	List<User> findAllAssistancesSince(String since, String until) throws ParseException;
	List<Assistance> findAllAssistances();
}
