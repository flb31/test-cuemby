package com.fabian.testcuemby;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestCuembyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestCuembyApplication.class, args);
	}
}
