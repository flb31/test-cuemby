package com.fabian.testcuemby.persistence;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.fabian.testcuemby.model.Position;
import com.fabian.testcuemby.model.User;
import com.fabian.testcuemby.util.FormatData;

public class UserPersistence {

	private List<User> users;
	private List<Position> positions;
	
	public UserPersistence() {
		PositionPersistence pp = new PositionPersistence();
		positions = pp.getPositions();
		
		this.users = build();
	}
	
	public List<User> getUser() {
		return this.users;
	}
	
	protected List<User> build() {
		ArrayList<User> list = new ArrayList<>(); 
		try {
			Position position = positions.get(0);
			list.add(new User(1, "1128053244", "Fabian", "Luna", position, "31-03-1987", "01-05-2016", User.ACTIVE, position.getId()));
			
			position = positions.get(2);
			list.add(new User(2, "12345678", "Juan", "Perez", position, "01-01-1990", "01-05-2015", User.ACTIVE, position.getId()));
			
			position = positions.get(1);
			list.add(new User(3, "12345678", "Alejandra", "Rios", position, "01-01-1992", "01-01-2017", User.ACTIVE, position.getId()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
	}
	
}
