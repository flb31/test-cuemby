package com.fabian.testcuemby.persistence;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.fabian.testcuemby.model.Assistance;
import com.fabian.testcuemby.model.User;
import com.fabian.testcuemby.util.FormatData;

public class AssistancePersistence {
	private List<Assistance> assistances;
	private List<User> users;
	
	public AssistancePersistence() {
		UserPersistence up = new UserPersistence();
		users = up.getUser();
		this.assistances = build();
	}
	
	public List<Assistance> getAssistances() {
		return this.assistances;
	}
	
	protected List<Assistance> build() {
		ArrayList<Assistance> list = new ArrayList<>(); 
		// just examples
		int i = 0;
		
		while(++i <= 100) {
			int year = getRandom(2015, 2017);
			int month = getRandom(1, 12);
			int day = getRandom(1, 28);
			
			String monthStr =  numTwoDig(month);
			String dayStr =  numTwoDig(day);
			
			String date = dayStr + "-"+ monthStr + "-" + year;
			
			User user = users.get(getRandom(0, 2));
			
			try {
				list.add(new Assistance(FormatData.randomID(), user, date, user.getId()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return list;
	}
	
	protected int getRandom(int min, int max) {
		int randomNum = min+(int)(Math.random()*((max-min) + 1));
		return randomNum;
	}
	
	protected String numTwoDig(int num) {
		return (num <= 9) ? "0" + num: ""+ num;
	}
}
