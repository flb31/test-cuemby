package com.fabian.testcuemby.persistence;

import java.util.ArrayList;
import java.util.List;

import com.fabian.testcuemby.model.Position;
import com.fabian.testcuemby.model.Salary;
import com.fabian.testcuemby.util.FormatData;

public class PositionPersistence {
	private List<Position> positions;
	private List<Salary> salaries;
	
	public PositionPersistence() {
		SalaryPersistence sp = new SalaryPersistence();
		salaries = sp.getSalaries();
		
		this.positions = build();
	}
	
	public List<Position> getPositions() {
		return this.positions;
	}
	
	protected List<Position> build() {
		ArrayList<Position> list = new ArrayList<>(); 
		// just examples
		Salary salary = salaries.get(2); 
		list.add(new Position(1,"Ingeniero de Sistemas", salary));
		
		salary = salaries.get(3); 
		list.add(new Position(2,"Lider Desarrollo", salary));
		
		salary = salaries.get(2); 
		list.add(new Position(3,"Desarrollador Junior", salary));
	
		return list;
	}
	
}
