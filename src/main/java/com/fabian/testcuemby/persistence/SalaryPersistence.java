package com.fabian.testcuemby.persistence;

import java.util.ArrayList;
import java.util.List;

import com.fabian.testcuemby.model.Salary;
import com.fabian.testcuemby.util.FormatData;

public class SalaryPersistence {

	private List<Salary> salaries;
	
	public SalaryPersistence() {
		this.salaries = buildSalaries();
	}
	
	public List<Salary> getSalaries() {
		return this.salaries;
	}
	
	protected List<Salary> buildSalaries() {
		
		ArrayList<Salary> list = new ArrayList<>(); 
		list.add(new Salary(1, "Salario Minimo", 730000));
		list.add(new Salary(2, "Desarrollador Junior", 1500000));
		list.add(new Salary(3, "Desarrollador Senior", 3000000));
		list.add(new Salary(4, "Lider de Proyecto", 400000));
		return list;
	}
}
