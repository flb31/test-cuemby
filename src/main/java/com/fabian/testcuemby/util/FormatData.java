package com.fabian.testcuemby.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

public class FormatData extends Validator {
	
	public static final int MAX_RANGE = 10000;
	public static final int MIN_RANGE = 1000;
	public static final String DATE_DD_MM_YYYY =  "dd-MM-yyyy";
	
	public static int calculateYears(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		
		LocalDate today = LocalDate.now();
		LocalDate birthday = LocalDate.of(year, month, day);
		
		Period p = Period.between(birthday, today);
		
		return p.getYears();
	}
	
	public static Date stringToDate(String dateStr) throws ParseException {
		DateFormat format = new SimpleDateFormat(DATE_DD_MM_YYYY);
		Date date = format.parse(dateStr);
		
		return date;
	}
	
	public static boolean isValidDate(String dateStr) {
		return Validator.dateddMMYYYY(dateStr, true);
	}
	
	public static boolean isValidDate(Date date) {
		try {
			String str = dateToString(date);
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return false;
		}
	}
	
	public static String dateToString(Date date) {
		DateFormat df = new SimpleDateFormat(DATE_DD_MM_YYYY); 
		String dateString = df.format(date);
		return dateString;
	}
	
	public static int randomID() {
		Random rand = new Random();
		int randomNum = rand.nextInt((MAX_RANGE - MIN_RANGE) + 1) + MIN_RANGE;
		Calendar c = Calendar.getInstance(); 
		int seconds = c.get(Calendar.SECOND);
		String uniqueValue = randomNum +""+ seconds;
		
		return Integer.parseInt(uniqueValue);
	}
}
