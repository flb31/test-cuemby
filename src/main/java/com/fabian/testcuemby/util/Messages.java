package com.fabian.testcuemby.util;

public abstract class Messages {
	protected final String MESSAGE_NOT_VALID= "%s not valid. %s";
	protected final String MESSAGE_REQUIRED= "%s is required.";
	protected final String MESSAGE_MIN_REQUIRED = "%s is required. Min %d characters.";
	protected final String MESSAGE_SUCCESS = "%s registered successfully.";
	protected final String MESSAGE_UPDATE_SUCCESS = "%s updated successfully.";
	protected final String MESSAGE_NOT_FOUND = "%s not exist.";
	protected final String MESSAGE_NOT_UPDATED = "%s not updated.";
	protected final String MESSAGE_IDS = "%s can't updated. ID not valid.";
	protected final String MESSAGE_EXIST = "%s exist %s";
}
