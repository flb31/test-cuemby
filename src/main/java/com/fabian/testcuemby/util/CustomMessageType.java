package com.fabian.testcuemby.util;

public class CustomMessageType {
	private String message;
	
	public CustomMessageType(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
