package com.fabian.testcuemby.util;

public class Validator extends Messages {
	
	// Regex
	protected static final String TEXT = "[\\w\\s\\p{L}]+";
	protected static final String EMPTY_STRING = TEXT + "{3,}";
	protected static final String EMAIL = "^[a-zA-Z]+[\\w\\.\\-]*@[a-zA-Z]+(\\.{1}[a-zA-Z]{2,4})+$";
	protected static final String DATE_dd_MM_YYYY = "^\\d{2}-\\d{2}-\\d{4}$";
	protected static final String NUMBER = "[0-9]+";
	
	protected static final int MIN_CHAR_ID = 7;

    public static boolean empty(String input, boolean required) {
        return input != null && validate(EMPTY_STRING, input, required);
    }
    
    public static boolean text(String input, boolean required) {
    		return validate(TEXT, input, required);
    }
    
    public static boolean minCharacters(String input, boolean required, int minCharacters) {
    		final String EMPTY_ID = String.format("\\w{%d,}", minCharacters);
    		return validate(EMPTY_ID, input, required);
    }

    public static boolean email(String input, boolean required) {
        return validate(EMAIL, input, required);
    }

    public static boolean dateddMMYYYY(String input, boolean required) {
        return validate(DATE_dd_MM_YYYY, input, required);
    }

    protected static boolean number(float input, boolean required) {
        return validate(NUMBER, input, required);
    }

    public static boolean validate(String regex, Object input, boolean required) {
        String value = String.valueOf(input);
        boolean flag = true;
        if(isRequired(required, value) ) {
            flag = validate(regex, value);
        }

        return flag;
    }

    public static boolean isRequired(boolean required, String value) {
        return required || value.length() > 0;
    }

    protected static boolean validate(String reg, String value) {
        return value.matches(reg);
    }
    
    public static String message(String typeMessage, Object... args) {
    		return String.format(typeMessage, args);
    }
}
